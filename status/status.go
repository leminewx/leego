package status

const (
	STATUS_FORBIDDEN             = "403 forbidden"
	STATUS_METHOD_NOT_ALLOWED    = "405 method not allowed"
	STATUS_INTERNAL_SERVER_ERROR = "500 internal server error"
	STATUS_BAD_GATEWAY           = "502 bad gateway"
)
